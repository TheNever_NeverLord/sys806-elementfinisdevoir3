from re import T
from readfile import *
from parameter import *
from femClass import *
from exportSolution import *
from commun import *
import matplotlib.pyplot as plt

def solexacteQ4(filename):
  preProcessData(filename)
  nodeL2, connectionL2, convectionL2, dirichletTempL2 = extractPreProcessData()
  matriceConnectionL2 = assembleMatriceConnection2D(connectionL2, convectionL2)
  dirichlet = conditionLimite(dirichletTempL2)
  totalNumberOfNode = nodeL2.shape[0]
  numberOfElement = connectionL2.shape[0]
  numberOfBorderElement = convectionL2.shape[0]
  mesh = Mesh(nodeL2,matriceConnectionL2,numberOfElement,numberOfBorderElement,False)
  ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
  femL2 = Fem(mesh,ndlt,dirichlet,False)
  femL2 =resolveproblem(femL2)
  return femL2


def solexacteT3(filename):
  preProcessData(filename)
  nodeL2, connectionL2, convectionL2, dirichletTempL2 = extractPreProcessData()
  matriceConnectionL2 = assembleMatriceConnection2D(connectionL2, convectionL2)
  dirichlet = conditionLimite(dirichletTempL2)
  totalNumberOfNode = nodeL2.shape[0]
  numberOfElement = connectionL2.shape[0]
  numberOfBorderElement = convectionL2.shape[0]
  mesh = Mesh(nodeL2,matriceConnectionL2,numberOfElement,numberOfBorderElement,True)
  ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
  femL2 = Fem(mesh,ndlt,dirichlet,True)
  femL2 =resolveproblem(femL2)
  return femL2


def calculErrorQuadQ4(filenameL2):
  femL2 = solexacteQ4(filenameL2)
  err = []
  for filename in listQ4errL2:
    preProcessData(filename)
    node, connection, convection, dirichletTemp = extractPreProcessData()
    matriceConnection = assembleMatriceConnection2D(connection, convection)
    dirichlet = conditionLimite(dirichletTemp)
    totalNumberOfNode = node.shape[0]
    numberOfElement = connection.shape[0]
    numberOfBorderElement = convection.shape[0]
    mesh = Mesh(node,matriceConnection,numberOfElement,numberOfBorderElement,False)
    ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
    fem = Fem(mesh,ndlt,dirichlet,False)
    fem =resolveproblem(fem)
    index =[]
    for i in range(0,fem.mesh.numberOfNodes):
      for j in range (0,femL2.mesh.numberOfNodes):
        if(fem.mesh.nodes[i,0] == femL2.mesh.nodes[j,0] and fem.mesh.nodes[i,1] == femL2.mesh.nodes[j,1]):
          index.append([i,j])
    index = np.array(index)
    index = index.astype('uint8')
    sum = 0
    for e in range(0,index.shape[0]):
      sum = sum + (fem.solution[index[e,0]]-femL2.solution[index[e,1]])**2
    err.append(sqrt(sum))
  for i in range (0, len(err)):
   err[i] = log10(err[i])
   nQ4[i] = log10(nQ4[i])
  
  tmp = np.polyfit(nQ4,err,1)
  print("ordre de convergence log10", -tmp[0])
  plt.figure(num='erreur en fonction du nombre d\'element Q4')
  plt.plot(nQ4,err,label="erreur L2 element Q4")
  plt.legend()
  plt.show()


def calculErrorQuadT3(filenameL2):
  femL2 = solexacteT3(filenameL2)
  err = []
  for filename in listT3errL2:
    preProcessData(filename)
    node, connection, convection, dirichletTemp = extractPreProcessData()
    matriceConnection = assembleMatriceConnection2D(connection, convection)
    dirichlet = conditionLimite(dirichletTemp)
    totalNumberOfNode = node.shape[0]
    numberOfElement = connection.shape[0]
    numberOfBorderElement = convection.shape[0]
    mesh = Mesh(node,matriceConnection,numberOfElement,numberOfBorderElement,True)
    ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
    fem = Fem(mesh,ndlt,dirichlet,True)
    fem =resolveproblem(fem)
    index =[]
    for i in range(0,fem.mesh.numberOfNodes):
      for j in range (0,femL2.mesh.numberOfNodes):
        if(fem.mesh.nodes[i,0] == femL2.mesh.nodes[j,0] and fem.mesh.nodes[i,1] == femL2.mesh.nodes[j,1] and fem.mesh.nodes[i,2] == femL2.mesh.nodes[j,2]):
          index.append([i,j])

    index = np.array(index)
    index = index.astype('uint8')
    sum = 0
    for e in range(0,index.shape[0]):
      sum = sum + (fem.solution[index[e,0]]-femL2.solution[index[e,1]])**2
    err.append(sqrt(sum))
  for i in range (0, len(err)):
   err[i] = log10(err[i])
   nT3[i] = log10(nT3[i])
  
  tmp = np.polyfit(nT3,err,1)
  print("ordre de convergence log10", -tmp[0])
  plt.figure(num='erreur en fonction du nombre d\'element maillage T3')
  plt.plot(nT3,err,label="erreur L2 element T3")
  plt.legend()
  plt.show()