# Parameters
kx = 170.0
ky = 170.0  # W/m K
h = 150.0 # coef of convection
Tex = 22.0
T0 = 100.0
e = 0.02

listT3errL2 =['../maillage/triangle/T3_2Elements.dat', '../maillage/triangle/T3_8Elements.dat', '../maillage/triangle/T3_18Elements.dat', 
              '../maillage/triangle/T3_32Elements.dat', '../maillage/triangle/T3_50Elements.dat', '../maillage/triangle/T3_72Elements.dat',
              '../maillage/triangle/T3_98Elements.dat', '../maillage/triangle/T3_128Elements.dat']

nT3 =[2,8,18,32,50,72,98,128]
nQ4 =[4,9,16,25,36,49,64,81,100,121]
listQ4errL2 =['../maillage/quadrangle/Q4_4Elements.dat', '../maillage/quadrangle/Q4_9Elements.dat', '../maillage/quadrangle/Q4_16Elements.dat',
              '../maillage/quadrangle/Q4_25Elements.dat', '../maillage/quadrangle/Q4_36Elements.dat', '../maillage/quadrangle/Q4_49Elements.dat',
              '../maillage/quadrangle/Q4_64Elements.dat', '../maillage/quadrangle/Q4_81Elements.dat', '../maillage/quadrangle/Q4_100Elements.dat', 
              '../maillage/quadrangle/Q4_121Elements.dat']