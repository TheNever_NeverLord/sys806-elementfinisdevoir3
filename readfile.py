import numpy as np
from parameter import *

def openPreProcessFileNode(filename):
  t = open(filename,'r')
  connection = np.loadtxt(filename,dtype=np.float64)[:,1:]
  t.close()
  return connection

def openPreProcessFile(filename):
  t = open(filename,'r')
  connection = np.loadtxt(filename,dtype=np.float64)
  t.close()
  return connection

def writeFile(filename,data):
  inputData = open(filename,'w')
  for i in range (len(data)):
    inputData.write(str(data[i]))
  inputData.close()

def extractNode(f):
  lookup = 'nblock'
  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  noeuds = []
  lookup = '-1'
  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    noeuds.append(line)
  return noeuds

def extractConnection(f):
  lookup = 'eblock'
  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  matriceConnection = []
  lookup = '-1'
  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    matriceConnection.append(line)
  return matriceConnection

def extractDirichlet(f):
  lookup = 'Define Temperature Constraint'
  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  f.readline()

  dirichletX = []
  temp = []

  lookup = '-1'
  for num, line in enumerate(f, 1):
    if 'fcum' in line:
      break
    temp.append(line)

  tempString = ""

  for i in range(0,num-1):
    if(i==num-2):
      tempString = tempString +temp[i]
    else :
      tempString = tempString +temp[i].rstrip()
      
  dirichletX.append(tempString)
  return dirichletX

def extractConvection(f):
  lookup = 'Create "Convection"'
  for num, line in enumerate(f, 1):
    if lookup in line:
      break
  f.readline()
  f.readline()
  f.readline()
  f.readline()
  matriceConvection = []
  lookup = '-1'
  for num, line in enumerate(f, 1):
    if '-1' in line:
      if(line.find('1') == 1):
        break
    matriceConvection.append(line)
  return matriceConvection

def preProcessData(filename):
  filename0 = "noeuds.txt"
  filename1 = "connection.txt"
  filename2 = "convection.txt"
  filename3 = "dirichlet.txt"

  inputData = open(filename,'r',encoding = "ISO-8859-1")

  noeuds = extractNode(inputData)
  matriceConnection = extractConnection(inputData)
  dirichletX = extractDirichlet(inputData)
  matriceConvection = extractConvection(inputData)

  inputData.close()

  writeFile(filename0,noeuds)
  writeFile(filename1,matriceConnection)
  writeFile(filename2, matriceConvection)
  writeFile(filename3,dirichletX)

def extractPreProcessData():
  filename0 = "noeuds.txt"
  filename1 = "connection.txt"
  filename2 = "convection.txt"
  filename3 = "dirichlet.txt"
  node = openPreProcessFileNode(filename0)
  connection = openPreProcessFile(filename1)
  convection = openPreProcessFile(filename2)
  dirichletTemp = openPreProcessFile(filename3)
  return node , connection, convection, dirichletTemp

def conditionLimite(dirichletTemp):
  temp = len(dirichletTemp)
  T_Const = np.zeros((temp,2))
  for i in range(temp):
    T_Const[i] = dirichletTemp[i]-1,T0
  return T_Const

def assembleMatriceConnection2D(connection, convection):
  #concatenation des matrices connection et convection
  matConnec = np.zeros((connection.shape[0]+convection.shape[0],connection.shape[1]-11))
  for i in range(connection.shape[0]):
    #pour les quad lin en
    if(connection[i,8] == 4 and connection[i,13] != connection[i,14]):
      matConnec[int(connection[i,10]-1),0::] = connection[i,11::]-1

    #pour les quad quadratique incomplet
    if(connection[i,8] == 8):
      matConnec[int(connection[i,10]-1),0::] = connection[i,11::]-1

    #pour les triangle lin
    if(connection[i,8] == 4 and connection[i,13] == connection[i,14] ):
      matConnec[int(connection[i,10]-1),0::] = connection[i,11::]-1

    #pour les triangle quadratique
    if(connection[i,8] == 6):
      matConnec[int(connection[i,10]-1),0::] = connection[i,11::]-1

  #pour ligne interpolation quadratique
  if(convection.shape[1] == 8):
    for i in range(convection.shape[0]):
      matConnec[int(convection[i,0]-1),0:3] = convection[i,5::]-1

  #pour ligne interpolation lineaire
  if(convection.shape[1] == 7):
    for i in range(convection.shape[0]):
      matConnec[int(convection[i,0]-1),0:2] = convection[i,5::]-1
  return matConnec