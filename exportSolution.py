from os import linesep as endl

def export_mesh(mesh):        
  filename = "mesh.vtk"
  with open(filename,"w") as f:
    f.write("# vtk DataFile Version 1.0" + endl)
    f.write("PlaneStress Mesh SYS806" + endl)
    f.write("ASCII" + endl)
    f.write("DATASET UNSTRUCTURED_GRID" + endl)
    f.write("POINTS " +  str(mesh.numberOfNodes) + " float" + endl)
    for i in range(0,mesh.numberOfNodes):
      f.write(str(mesh.nodes[i][0]) + " " + str(mesh.nodes[i][1]) + " " + str(mesh.nodes[i][2]) + endl)
    f.write(endl + "CELLS " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + " " + str(mesh.numberOfElement*(mesh.numberOfNodePerElement+1)+mesh.numberOfBorderElement*(mesh.numberOfNodePerBorderElement+1)) + endl)
    for i in range(0,mesh.numberOfElement):
      f.write(str(mesh.numberOfNodePerElement) + " " ) 
      for j in range(mesh.numberOfNodePerElement):
          f.write(str(mesh.matriceConnection[i][j]) + " ")
      f.write(endl)
    for i in range(0,mesh.numberOfBorderElement):
      f.write(str(mesh.numberOfNodePerBorderElement) + " " ) 
      for j in range(mesh.numberOfNodePerBorderElement):
          f.write(str(mesh.matriceConnection[i+mesh.numberOfElement][j+mesh.numberOfElement]) + " ")
      f.write(endl)
    f.write(endl + "CELL_TYPES " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + endl)
    if(mesh.numberOfNodePerElement == 3):
      cellType_elem=5; #Triangle
    elif(mesh.numberOfNodePerElement == 4):
      cellType_elem=9; #Quadrangle
    else:
      raise Exception("ELEM CELL TYPE ERROR")
    if(mesh.numberOfNodePerBorderElement == 2):
      cellType_elem_contour=3; #Line
    else:
      raise Exception("ELEM CONTOUR CELL TYPE ERROR")
    for i in range(0,mesh.numberOfElement):
      f.write(str(cellType_elem) + endl)
    for i in range(0,mesh.numberOfBorderElement):
      f.write(str(cellType_elem_contour) + endl)

def export_sol_vtk(mesh, T):        
  filename = "mesh_sol.vtk"
  with open(filename,"w") as f:
    f.write("# vtk DataFile Version 1.0" + endl)
    f.write("Temperature SYS807" + endl)
    f.write("ASCII" + endl)
    f.write("DATASET UNSTRUCTURED_GRID" + endl)
    f.write("POINTS " +  str(mesh.numberOfNodes) + " float" + endl)
    for i in range(0,mesh.numberOfNodes):
      f.write(str(mesh.nodes[i][0]) + " " + str(mesh.nodes[i][1]) + " " + str(mesh.nodes[i][2]) + endl)

    f.write(endl + "CELLS " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + " " + str(mesh.numberOfElement*(mesh.numberOfNodePerElement+1)+mesh.numberOfBorderElement*(mesh.numberOfNodePerBorderElement+1)) + endl)
    for i in range(0,mesh.numberOfElement):
      f.write(str(mesh.numberOfNodePerElement) + " " ) 
      for j in range(mesh.numberOfNodePerElement):
          f.write(str(int(mesh.matriceConnection[i][j])) + " ")
      f.write(endl)
    for i in range(0,mesh.numberOfBorderElement):
      f.write(str(int(mesh.numberOfNodePerBorderElement)) + " " ) 
      for j in range(int(mesh.numberOfNodePerBorderElement)):
          f.write(str(int(mesh.matriceConnection[i+mesh.numberOfElement][j])) + " ")
      f.write(endl)
      
    f.write(endl + "CELL_TYPES " + str(mesh.numberOfElement+mesh.numberOfBorderElement) + endl)
    if(mesh.numberOfNodePerElement == 3):
      cellType_elem=5; #Triangle
    elif(mesh.numberOfNodePerElement == 4):
      cellType_elem=9; #Quadrangle
    else:
      raise Exception("ELEM CELL TYPE ERROR")
    if(mesh.numberOfNodePerBorderElement == 2):
      cellType_elem_contour=3; #Line
    else:
      raise Exception("ELEM CONTOUR CELL TYPE ERROR")
    for i in range(0,mesh.numberOfElement):
      f.write(str(cellType_elem) + endl) 
    for i in range(0,mesh.numberOfBorderElement):
      f.write(str(cellType_elem_contour) + endl) 
    f.write(endl + "POINT_DATA " + str(mesh.numberOfNodes) + endl)
    f.write("Scalars Temperature float" + endl)
    f.write("LOOKUP_TABLE default" + endl)
    for i in range(0,mesh.numberOfNodes):
      f.write(str(T[i]) + endl)
