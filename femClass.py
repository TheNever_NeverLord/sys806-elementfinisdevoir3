from math import *
import numpy as np
import sys
from parameter import *
from commun import *

class Mesh():
  numberFreedomDegreesPerNode = 1
  numberOfNodePerBorderElement = 2
  def __init__(self,node,matriceConnection,numberOfElement,numberOfBorderElement,meshType):
    self.nodes = node
    self.matriceConnection = matriceConnection
    self.numberOfElement = numberOfElement
    self.numberOfBorderElement = numberOfBorderElement
    self.numberOfNodes = node.shape[0]
    if(meshType == True):
      self.numberOfNodePerElement = 3
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
    else:
      self.numberOfNodePerElement = 4
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement

class Fem():
  numberFreedomDegreesPerNode = 1
  numberFreedomDegreesPerBorderNode = 1
  numberOfNodePerBorderElement = 2
  Nuic = numberFreedomDegreesPerBorderNode*numberOfNodePerBorderElement

  def __init__(self, mesh, ndlt, dirichlet, meshType):
    self.mesh = mesh                    
    self.ndlt = ndlt
    self.dirichlet = dirichlet
    self.isT3 = meshType
    # Matrice du système
    self.K = np.zeros((self.ndlt, self.ndlt), dtype=np.float64)
    self.F = np.zeros(self.ndlt, dtype=np.float64)
    self.KInternalNode = np.zeros((self.ndlt, self.ndlt), dtype=np.float64)
    self.FInternalNode = np.zeros((self.ndlt, self.ndlt), dtype=np.float64)
    self.D=np.array([[kx,0],[0,ky]], dtype=np.float64)
    self.jacobian = 0
    self.determinantJacobian = 0
    self.inverseJacobain = 0
    self.solution = np.zeros(self.mesh.numberOfNodes, dtype=np.float64)
    self.ksi, self.eta, self.w = 0,0,0
    if(self.isT3 == True):
      self.gaussPointNumber = 4
      self.numberOfNodePerElement = 3
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
    else:
      self.gaussPointNumber = 7
      self.numberOfNodePerElement = 4
      self.Nui = self.numberFreedomDegreesPerNode*self.numberOfNodePerElement
    self.gauss2D()

  def gauss1D(self,npg):
    if(npg ==1):
      self.ksi1D = np.array([0])
      self.w1D = np.array([2])
    elif(npg ==2):
      self.ksi1D = np.array([-1/sqrt(3),1/sqrt(3)])
      self.w1D = np.array([1,1])
    elif(npg ==3):
      self.ksi1D = np.array([0,-sqrt(3/5.),sqrt(3/5.)])
      self.w1D = np.array([8/9.,5/9.,5/9.])
    elif(npg ==4):
      self.ksi1D = np.array([sqrt((3-2*sqrt(6/5.))/7.),-sqrt((3-2*sqrt(6/5.))/7.),sqrt((3+2*sqrt(6/5.))/7.),-sqrt((3+2*sqrt(6/5.))/7.)])
      self.w1D = np.array([0.5+(1/6.)*1./sqrt(6/5.),0.5+(1/6.)*1./sqrt(6/5.),0.5-(1/6.)*1./sqrt(6/5.),0.5-(1/6.)*1./sqrt(6/5.)])
      return self.ksi1D , self.w1D
    else:
      sys.exit(f"{bcolors.FAIL}error value not expected, expected 1,2,3,4 in gauss integration 1D{bcolors.ENDC}")


  def gauss2D(self):
    if(self.isT3 == True):
      if(self.gaussPointNumber == 3):
        self.w = np.array([1/6, 1/6, 1/6], dtype=np.float64)
        self.ksi = np.array([1/6,2/3,1/6])
        self.eta = np.array([1/6,1/6,2/3])
      elif(self.gaussPointNumber == 4):
        self.w = np.array([-27/96, 25/96, 25/96, 25/96])
        self.ksi = np.array([1/3, 1/5, 3/5, 1/5])
        self.eta = np.array([1/3, 1/5, 1/5, 3/5]) 
      else:
        sys.exit(f"{bcolors.FAIL}error value not expected T3, expected 3,4 in gauss integration 2D{bcolors.ENDC}")
    else:
      if(self.gaussPointNumber == 4):
        self.w = np.array([1, 1, 1, 1], dtype=np.float64)
        temp = 1./sqrt(3.)
        self.ksi = np.array([temp, temp, -temp, -temp])
        self.eta = np.array([temp, -temp, -temp, temp])
      elif(self.gaussPointNumber == 7):
        temp = 1./sqrt(3/5)
        self.w = np.array([8/7, 20/63, 20/63, 20/36, 20/36, 20/36, 20/36])
        self.ksi = np.array([0,0,0, temp, -temp, -temp, temp])
        self.eta = np.array([0, sqrt(14/15), -sqrt(14/15), temp, temp, -temp, -temp])
      else:
        sys.exit(f"{bcolors.FAIL}error value not expected Q4, expected 4,7 in gauss integration 2D{bcolors.ENDC}")


  def feeldof(self, nd, numberOfNodePerElement, numberFreedomDegreesPerNode):
    edof = numberOfNodePerElement*numberFreedomDegreesPerNode
    index = np.zeros(edof, dtype=np.int32)
    k=0
    for i in range(0,numberOfNodePerElement):
      start = nd[i]*numberFreedomDegreesPerNode
      for j in range(0,numberFreedomDegreesPerNode):
        index[k] = start+j
        k=k+1
    return(index)
  
  def assemble(self, ke, fe, index):
    edof = len(index)
    for i in range(0, edof):
      i_index = index[i]
      for j in range(0, edof):
        j_index=index[j]
        self.K[i_index,j_index]=self.K[i_index,j_index]+ke[i,j]
      self.F[i_index] = self.F[i_index]+fe[i]

  def assembleMatrixInternalNode(self, nd, ke, fe):
    Kloc = self.feeldof(nd,self.numberOfNodePerElement,self.numberFreedomDegreesPerNode)
    self.assemble(ke,fe,Kloc)
    
  def assembleMatrixBorderNode(self, nd, ke, fe):
    Kloc = self.feeldof(nd,self.numberOfNodePerBorderElement,self.numberFreedomDegreesPerBorderNode)
    self.assemble(ke,fe,Kloc)


  def BKsiEta(self, ksi, eta):
    if (self.isT3 == True):
      Bke = np.array([[-1, 1, 0],[-1, 0, 1]], dtype=np.float64)
    else :
      Bke = np.array([[(eta-1)/4., (1-eta)/4., (1+eta)/4., -(1+eta)/4.],[(ksi-1)/4., -(1+ksi)/4., (1+ksi)/4., (1-ksi)/4.]], dtype=np.float64)
    return(Bke)

  def computeJacobian(self, i, coor):
    Bke = self.BKsiEta(self.ksi[i],self.eta[i])
    self.jacobian = np.matmul(Bke,coor)
    self.inverseJacobain = np.linalg.inv(self.jacobian) 
    self.determinantJacobian = np.linalg.det(self.jacobian)
    return Bke 

  def digitalIntegralInternalNode(self,coor):
    ke = np.zeros((self.Nui,self.Nui), dtype=np.float64)
    fe = np.zeros(self.Nui, dtype=np.float64)
    for i in range (0,self.gaussPointNumber):
      G = self.computeJacobian(i, coor)
      Bref = np.matmul(self.inverseJacobain,G)
      f = self.shapeFunction2D(self.ksi[i],self.eta[i])
      ke = ke + kx*self.determinantJacobian*self.w[i]*np.dot(Bref.transpose(),Bref) + (2*h/e)*self.determinantJacobian*self.w[i]*np.tensordot(f,f,0)
      fe = fe + (2*h*Tex/e)*self.determinantJacobian*self.w[i]* f
    return ke,fe

  def computeCoordonate(self, ie):
    nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
    coor = np.zeros((self.numberOfNodePerElement,2), dtype=np.float64)
    for i in range(0,self.numberOfNodePerElement):
      nd[i] = self.mesh.matriceConnection[ie,i]
      coor[i,0] = self.mesh.nodes[nd[i],0]  
      coor[i,1] = self.mesh.nodes[nd[i],1]
    return coor, nd

  def shapeFunction2D(self, ksi, eta):
    if(self.isT3 == True):
      N1 = (1-ksi-eta)
      N2 = ksi
      N3 = eta
      return np.array([N1,N2,N3], dtype=np.float64)
    else :
      N1 = (1-ksi-eta+ksi*eta)/4.
      N2 = (1+ksi-eta-ksi*eta)/4.
      N3 = (1+ksi+eta+ksi*eta)/4.
      N4 = (1-ksi+eta-ksi*eta)/4.
      return np.array([N1,N2,N3,N4], dtype=np.float64)

  def computeInternalNodes(self):
    ke = np.zeros((self.Nui,self.Nui), dtype=np.float64)
    fe = np.zeros((self.Nui,self.Nui), dtype=np.float64)
    for ie in range(0, self.mesh.numberOfElement):
      nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
      coor = np.zeros((self.numberOfNodePerElement,2), dtype=np.float64)
      coor, nd =self.computeCoordonate(ie)
      ke,fe = self.digitalIntegralInternalNode(coor)
      self.assembleMatrixInternalNode(nd, ke, fe)

  def shapeFunction1D(self,ksi):
    N1  =  (1-ksi)/2.
    N2  =  (1+ksi)/2.
    return np.array([N1,N2])

  def computeCauchyBoundaryConditionNodes(self):
    npg=4
    ksi,w1 =self.gauss1D(npg) 
    for ie in range(self.mesh.numberOfElement,self.mesh.numberOfElement+self.mesh.numberOfBorderElement):
      ke = np.zeros((self.Nuic,self.Nuic), dtype=np.float64)
      fe = np.zeros(self.Nuic, dtype=np.float64)
      nd = np.zeros(self.numberOfNodePerElement, dtype=np.int32)
      coor = np.zeros((self.numberOfNodePerElement,2), dtype=np.float64)

      for i in range(0,self.numberOfNodePerBorderElement):
          nd[i] = self.mesh.matriceConnection[ie,i]
          coor[i,0] = self.mesh.nodes[nd[i],0]  
          coor[i,1] = self.mesh.nodes[nd[i],1]  
      
      Le = sqrt((coor[0,0]-coor[1,0])**2 + (coor[0,1]-coor[1,1])**2)
      determinantJacobian=Le/2.
      for ig in range(0,npg):
        f = self.shapeFunction1D(ksi[ig])
        ke = ke + h*determinantJacobian*w1[ig]*np.tensordot(f,f,0) 
        fe = fe + (h*Tex)*determinantJacobian*w1[ig]* f
      self.assembleMatrixBorderNode(nd,ke,fe)
    

  def solve(self):
    self.solution = np.linalg.solve(self.K,self.F)
    
  def dirichletBoundaryConditions(self):
    for i in range(0,self.dirichlet.shape[0]):
      nn = self.dirichlet[i,0]
      nn= int(nn)
      for j in range(0,self.ndlt):
        if j==nn:
          self.K[j,j]= 1.0
          self.F[nn]= self.dirichlet[i,1]
        else:
          self.K[nn,j]=0.0