from readfile import *
from parameter import *
from femClass import *
from exportSolution import *
from commun import *
from femL2Error import *
import os

def main():
  filename, errL2, meshType = get_program_parameters()
  if 'Q4_1Elements.dat' in filename:
    sys.exit(f"{bcolors.FAIL}error value not expected, expected Q4_nElements.dat for n more than 1{bcolors.ENDC}")
  
  if(errL2 == 'false'): 
    preProcessData(filename)
    node, connection, convection, dirichletTemp = extractPreProcessData()
    matriceConnection = assembleMatriceConnection2D(connection, convection)
    dirichlet = conditionLimite(dirichletTemp)
    totalNumberOfNode = node.shape[0]
    numberOfElement = connection.shape[0]
    numberOfBorderElement = convection.shape[0]
    

  if(meshType == 'T3'):
    if 'Q4' in filename :
      sys.exit(f"{bcolors.WARNING}error value not expected, expected T3_nElements.dat{bcolors.ENDC}")

    if(errL2 == 'true'):
      filename = "../maillage/triangle/T3_1058Elements.dat"
      calculErrorQuadT3(filename)
    else:
      mesh = Mesh(node,matriceConnection,numberOfElement,numberOfBorderElement,True)
      ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
      fem = Fem(mesh,ndlt,dirichlet,True)
      fem =resolveproblem(fem)
      export_sol_vtk(mesh,fem.solution)
      if os.name != 'nt':
        os.system('./script.sh')
        pass

  elif (meshType == 'Q4'):
    if 'T3' in filename :
      sys.exit(f"{bcolors.WARNING}error value not expected, expected Q4_nElements.dat{bcolors.ENDC}")

    if(errL2 == 'true'):
      filename = "../maillage/quadrangle/Q4_1024Elements.dat"
      calculErrorQuadQ4(filename)
      
    else:
      mesh = Mesh(node,matriceConnection,numberOfElement,numberOfBorderElement,False)
      ndlt = totalNumberOfNode*mesh.numberFreedomDegreesPerNode
      fem = Fem(mesh,ndlt,dirichlet,False)
      fem =resolveproblem(fem)
      export_sol_vtk(mesh,fem.solution)
      if os.name != 'nt':
        os.system('./script.sh')
        pass
  

if __name__ == "__main__":
  main()
  